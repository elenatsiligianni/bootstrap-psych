<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Καλλιόπη Θανασούλα - Ψυχολόγος</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="ΚΑΛΛΙΟΠΗ,Καλλιόπη,ΘΑΝΑΣΟΥΛΑ,Θανασούλα, ΨΥΧΟΘΕΡΑΠΕΙΑ, ΨΥΧΟΛΟΓΟΣ, ΧΑΛΑΝΔΡΙ, ΨΥΧΟΔΥΝΑΜΙΚΗ, ΨΥΧΟΘΕΡΑΠΕΥΤΡΙΑ,ΠΟΠΗ,Πόπη therapist, kalliopi, thanasoula, popi, chalandri, psycotherapist, counselling,consulting,Βόρεια,Ανατολικά, Προάστια, ψυχολόγος" />
  <meta name="description" content="Καλωσήρθατε στην ιστοσελίδα της Καλλιόπης Θανασούλα, Συμβουλευτικής Ψυχολόγου &amp; Ψυχοδυναμικής Ψυχοθεραπεύτριας. 
Ατομική Ψυχοθεραπεία Εφήβων και Ενηλίκων, Θεραπεία Ζεύγους, Θεραπεία Σεξουαλικών Δυσλειτουργιών αλλά και Άρθρα και Απαντήσεις στις Ερωτήσεις σας... για να ζούμε καλύτερα!" />
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="shortcut icon" href="images/favicon.ico" >
    <!-- HTML5 shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<div class="container">
            <!--   Header     -->
            <header class="row">
                <div class="col-sm-3 logo"><a href="index.html"><img src="images/logo.png" alt="Logo"></a></div>
                <div class="col-sm-9 tagline">Καλλιόπη Θανασούλα - Ψυχολόγος</div>
            </header>
        
            <!--     Navigation bar       -->
            <div class="row">
                <nav class="navbar navbar-default" role="navigation">
                  <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar">
                      <ul class="nav navbar-nav textsize">
                        <li><a href="index.html">Αρχική</a></li>
                        <li><a href="bio.html">Βιογραφικό</a></li>
                        <li><a href="services.html">Υπηρεσίες</a></li>
                        <li><a href="faq.html">Συχνές Ερωτήσεις</a></li>
                        <li class="active"><a href="contact.php">Επικοινωνία</a></li>
                      </ul>
                    </div><!-- end navbar-collapse -->
                  </div><!-- end container-fluid -->
                </nav>
            </div>
        
        <!--   second row (carousel + right panel)     -->
		<div class="row">
			<div class="col-md-10">

				<h2>Βρείτε μας στο χάρτη</h2>
                <br>
	            <div class="map">
                    <iframe width="100%" height="100%" frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3145.0840832749955!2d23.785138999999987!3d37.975167!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzfCsDU4JzMwLjYiTiAyM8KwNDcnMDYuNSJF!5e0!3m2!1sen!2sca!4v1434066263080">
                    </iframe>
                    <br>
                </div>
                <div class="container">
                    <div class="row">    
                        <div class="col-md-10">
                            <small><a target="_blank" href="https://www.google.com/maps/place/%CE%95%CE%B8%CE%BD%CE%B9%CE%BA%CF%8C+%CE%9C%CE%B5%CF%84%CF%83%CF%8C%CE%B2%CE%B9%CE%BF+%CE%A0%CE%BF%CE%BB%CF%85%CF%84%CE%B5%CF%87%CE%BD%CE%B5%CE%AF%CE%BF/@37.9779925,23.7839099,17z/data=!4m2!3m1!1s0x14a1bd3833da4d53:0x7ed7f192dc0c188a?hl=en-US" style="color:#602F8D;text-align:left">Μετάβαση σε μεγαλύτερο χάρτη</a>
                            </small> 
                        </div>
                    </div>
                </div>

			</div>
            <br>
            
            
            <!--   Right panel         -->
			<div class="col-md-2">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3>Διευθύνσεις</h3>
					</div>
					<div class="panel-body">
						<p><span class="glyphicon glyphicon-map-marker"></span> Διεύθυνση Γραφείου:</p>
                        <p>Maecenas dignissim finibus</p>
						<p>T.K. : 12345</p>
                        <p><span class="glyphicon glyphicon-phone-alt"></span> Τηλέφωνο:</p>
                        <p>2109876543</p>
                        <p><span class="glyphicon glyphicon-envelope"></span><a href="mailto:webmaster@example.com"> E-mail</a></p>
<!--                    <p class="responsive email">kalliopi@<br>gmail.com</p>-->
<!--                    <a href="mailto:webmaster@example.com">Jane Doe</a>-->
<!--						<p><a href="#" class="btn btn-primary">Read more </a></p>-->
                        
                        <hr>
                        <a href="https://www.facebook.com"><img src="images/fbook1.png" alt="Facebook"></a>
                        <a href="https://twitter.com"><img src="images/twitter1.png" alt="Twitter"></a>
                        <a href="https://www.linkedin.com"><img src="images/lin1.png" alt="LinkedIn"></a>
                        <a href="https://plus.google.com"><img src="images/gplus1.png" alt="GooglePlus"></a>
					</div>
				</div><!-- end panel -->
			</div>
		</div><!-- end of second row -->
        
        
        <br>
        <?php

		// Check for Header Injections
		function has_header_injection($str) {
			return preg_match( "/[\r\n]/", $str );
		}
		
		
		if (isset($_POST['submit'])) {
			
			// Assign trimmed form data to variables
			// Note that the value within the $_POST array is looking for the HTML "name" attribute, i.e. name="email"
			$name	= trim($_POST['name']);
			$email	= trim($_POST['email']);
			$msg	= $_POST['message']; // no need to trim message
		
			// Check to see if $name or $email have header injections
			if (has_header_injection($name) || has_header_injection($email)) {
				
				die(); // If true, kill the script
				
			}
			
            //never reaches because fields have [required] property
			if (!$name || !$email || !$msg) {
				echo '<h4 class="error">All fields required.</h4><a href="contact.php" class="button block">Go back and try again</a>';
				exit;
			}
			
			// Add the recipient email to a variable
			$to	= "elentsilig@hotmail.com";
			
			// Create a subject
			$subject = "$name sent a message via your contact form";
			
			// Construct the message
			$message .= "Name: $name\r\n";
			$message .= "Email: $email\r\n\r\n";
			$message .= "Message:\r\n$msg";
			

		
			$message = wordwrap($message, 72); // Keep the message neat n' tidy
		
			// Set the mail headers into a variable
			$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
			$headers .= "From: " . $name . " <" . $email . ">\r\n";
			$headers .= "X-Priority: 1\r\n";
			$headers .= "X-MSMail-Priority: High\r\n\r\n";
		
			
			// Send the email!
			mail($to, $subject, $message, $headers);
		?>
		
		<!-- Show success message after email has sent -->
       
<!--
            <h5>Ευχαριστούμε για το μήνυμά σας !</h5>
            <p>Θα επικοινωνήσουμε μαζί σας το συντομότερο δυνατόν.</p>
            <p><a href="index.html" class="btn btn-primary">&laquo; Μετάβαση στην αρχική σελίδα</a></p>
-->
        
        <?php
			} else {
		?>
        <div class="row">
               <form  method="post" action="contact.html">
                  <fieldset>
                          <legend>Έχετε ερωτήσεις;</legend>
                          <div class="form-group">
                               <label for="name" class="col-sm-2"> *Το όνομά σας:</label>
                               <div class="col-sm-10">
                                 <input type="text" name="name" class="form-control" required>
                               </div>
		                  </div>
                          <div class="form-group">
                              <label for="email" class="col-sm-2"> *Το email σας:</label>
                              <div class="col-sm-10">
                                <input type="text" name="email" class="form-control" placeholder="ex. name@domain.com" required>
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="message" class="control-label"> Σχόλια:</label>
                              <textarea name="message" class="form-control" rows="3" data-toggle="tooltip" title="Θα χαρούμε πολύ να ακούσουμε από εσάς!" data-placement="bottom"></textarea>
                          </div>

                          <p>Τα πεδία με αστερίσκο(*) είναι υποχρεωτικά.</p>
                          <div class="form-group">
                              <button type="submit" class="btn btn-primary" name="submit">Αποστολή</button>
                              <input  type="reset" value="Καθαρισμός φόρμας" />
                          </div>
                  </fieldset>
               </form>
            <?php
			  }
		      ?>

        </div>
      
        <!--   Footer     -->
		<footer class="row siteinfo">
			<p><a href="#"><span class="glyphicon glyphicon-arrow-up"></span>Επιστροφή στην κορυφή</a></p>
            <p>&copy; Copyright 2015, Καλλιόπη Θανασούλα. </p>
		</footer>
        
	</div><!-- end container -->

    
<!-- javascript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    	$("textarea").tooltip();
    	$(".form-group.collapse").collapse();
    </script>
</body>
</html>


<!--
		NOTE:
		In the form in contact.php, the name text field has the name "name"
		If the user submits the form, the $_POST['name'] variable will be
		automatically created, and will contain the text they typed into
		the field. The $_POST['email'] variable will contain whatever they typed
		into the email field.
	
	
		PHP used in this script:
		
		preg_match()
		- Perform a regular expression match
		- http://ca2.php.net/preg_match
		
		isset()
		- Determine if a variable is set and is not NULL
		- http://ca2.php.net/manual/en/function.isset.php
		
		$_POST
		- An associative array of variables passed to the current script via the HTTP POST method.
		- http://www.php.net/manual/en/reserved.variables.post.php
		
		trim()
		- Strip whitespace (or other characters) from the beginning and end of a string
		- http://www.php.net/manual/en/function.trim.php
		
		exit
		- Output a message and terminate the current script
		- http://www.php.net//manual/en/function.exit.php
		
		die()
		- Equivalent to exit
		- http://ca1.php.net/manual/en/function.die.php
		
		wordwrap()
		- Wraps a string to a given number of characters
		- http://ca1.php.net/manual/en/function.wordwrap.php
		
		mail()
		- Send mail
		- http://ca1.php.net/manual/en/function.mail.php
-->
