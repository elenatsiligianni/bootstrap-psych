<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Καλλιόπη Θανασούλα - Ψυχολόγος</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="shortcut icon" href="images/favicon.ico" >
</head>

<body>
	<div class="container">
            <!--   Header     -->
            <header class="row">
                <div class="col-sm-3 logo"><a href="index.html"><img src="images/logo.png" alt="Logo"></a></div>
                <div class="col-sm-9 tagline">Καλλιόπη Θανασούλα - Ψυχολόγος
                
                </div>
            </header>
        
            <!--     Navigation bar       -->
            <div class="row">
                <nav class="navbar navbar-default" role="navigation">
                  <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar">
                      <ul class="nav navbar-nav textsize">
                        <li class="active"><a href="index.html">Αρχική</a></li>
                        <li><a href="bio.html">Βιογραφικό</a></li>
                        <li><a href="services.html">Υπηρεσίες</a></li>
                        <li><a href="faq.html">Συχνές Ερωτήσεις</a></li>
                        <li><a href="contact.php">Επικοινωνία</a></li>
                      </ul>
                    </div><!-- end navbar-collapse -->
                  </div><!-- end container-fluid -->
                </nav>
            </div>
        
        <!--   second row (carousel + right panel)     -->
		<div class="row">
			<div class="col-md-10">
				<div id="imgs" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#imgs" data-slide-to="0" class="active"></li>
						<li data-target="#imgs" data-slide-to="1"></li>
						<li data-target="#imgs" data-slide-to="2"></li>
						<li data-target="#imgs" data-slide-to="3"></li>
						<li data-target="#imgs" data-slide-to="4"></li>
					</ol>
					<div class="carousel-inner">
						<div class="item active">
							<img src="images/big11.jpg" alt="">
						</div>
						<div class="item">
							<img src="images/big22.jpg" alt="">
						</div>
						<div class="item">
							<img src="images/big33.jpg" alt="">
						</div>
						<div class="item">
							<img src="images/big44.jpg" alt="">
						</div>
						<div class="item">
							<img src="images/big55.jpg" alt="">
						</div>
					</div>
					<a class="left carousel-control" href="#imgs" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left"></span>
					</a>
					<a class="right carousel-control" href="#imgs" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right"></span>
					</a>
				</div> <!-- end of carousel -->
				<h2>Καλωσήλθατε στον προσωπικό διαδικτυακό μου χώρο...</h2>
				<blockquote>
					<p>Nullam interdum posuere auctor. Quisque finibus pellentesque augue et laoreet. Nunc vulputate placerat accumsan. Sed vel porta nulla. Cras sed euismod quam. Vivamus ullamcorper fringilla ullamcorper. Fusce ultricies molestie tristique. Sed quis hendrerit felis, nec tincidunt felis. Cras viverra diam ornare augue cursus congue. Vestibulum turpis est, iaculis a gravida a, porta et ex. Aliquam et est neque. Nunc semper auctor risus, quis mollis mauris eleifend quis. Aliquam tempus leo rhoncus mattis pellentesque. Nunc non est metus. Aenean quis velit a tortor faucibus placerat in id lacus. Nunc feugiat sed felis et lacinia. </p>
                    <p>Cras eleifend metus id lectus scelerisque, ut sollicitudin mauris faucibus. Nulla ut ex euismod dui viverra tincidunt a sed sem. Vestibulum purus lorem, gravida commodo libero nec, ullamcorper euismod leo. Aliquam venenatis feugiat maximus. Cras rhoncus augue at mi porttitor, vel aliquet dui sodales. Nullam a felis lorem. Vestibulum convallis sed turpis ut posuere. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
					<small>Maecenas dignissim finibus placerat</small>
				</blockquote>

			</div>
            
            
            <!--   Right panel         -->
			<div class="col-md-2">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3>Διευθύνσεις</h3>
					</div>
					<div class="panel-body">
						<p><span class="glyphicon glyphicon-map-marker"></span> Διεύθυνση Γραφείου:</p>
                        <p>Maecenas dignissim finibus</p>
						<p>T.K. : 12345</p>
                        <p><span class="glyphicon glyphicon-phone-alt"></span> Τηλέφωνο:</p>
                        <p>2109876543</p>
                        <p><span class="glyphicon glyphicon-envelope"></span><a href="mailto:webmaster@example.com"> E-mail</a></p>
<!--                    <p class="responsive email">kalliopithanasoula@<br>gmail.com</p>-->
<!--                    <a href="mailto:webmaster@example.com">Jon Doe</a>-->
<!--						<p><a href="#" class="btn btn-primary">Read more </a></p>-->
                        
                        <hr>
                        <a href="https://www.facebook.com"><img src="images/fbook1.png" alt="Facebook"></a>
                        <a href="https://twitter.com"><img src="images/twitter1.png" alt="Twitter"></a>
                        <a href="https://www.linkedin.com"><img src="images/lin1.png" alt="LinkedIn"></a>
                        <a href="https://plus.google.com"><img src="images/gplus1.png" alt="GooglePlus"></a>
					</div>
				</div><!-- end panel -->
			</div>
		</div><!-- end of second row -->
        
        <!--   Footer     -->
		<footer class="row siteinfo">
			<p><a href="#"><span class="glyphicon glyphicon-arrow-up"></span> Back to top</a></p>
            <p>&copy; Copyright 2015, Καλλιόπη Θανασούλα. </p>
		</footer>
        
<!--
        <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2015 MyHotel. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
      </footer>   
-->
	</div><!-- end container -->

    
<!-- javascript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    	$('.carousel').carousel({
    		interval: 3500,
//    		wrap: false
    	});
    </script>
</body>
</html>